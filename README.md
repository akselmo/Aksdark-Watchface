# Aksdark-Watchface

Converted my KDE Plasma theme "Aksdark" to my Infinitime watchface. 
Uses parts of PineTime and Terminal watchfaces, basically I modified the whole thing on top of the two.. :D

[![Watchface](./screenshot.png)](./screenshot.png)

This repository has the couple files needed for the Watchface, except for the fonts.

The files go to the following folder: `InfiniTime/src/displayapp/screens/`

You also require the IBM Plex Mono font: [IBM Plex font repo](https://github.com/IBM/plex)

After downloading the fonts, you need add them to the InfiniTime files. Follow my fork (linked below) to see how it's done.

My branch where this watchface is in can be found from here: [My fork of InfiniTime with the Watchface branch](https://github.com/Akselmo/InfiniTime/tree/personal-watchface)

---

The InfiniTime repo can be found from here: [InfiniTime repo](https://github.com/InfiniTimeOrg/InfiniTime)

Link to patchfile: [Patch](https://github.com/InfiniTimeOrg/InfiniTime/compare/develop...Akselmo:InfiniTime:personal-watchface.patch)
